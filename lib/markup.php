<?php
	/**
	 * Functions relating to generating and returning/outputting markup
	 * 
	 * @since 0.1.0
	 */

	/**
	 * Outputs a favicon
	 * 
	 * @since 0.1.0
	 * 
	 * @see ucdf_get_favicon
	 * 
	 * @param string $uri @see ucdf_get_favicon
	 * @param string $base @see ucdf_get_favicon
	 * 
	 * @return void
	 */
	function ucdf_favicon( string $uri, string $base = '' ) : void {
		echo ucdf_get_favicon( $uri );
	}
		/**
		 * Gets a favicon
		 * 
		 * @since 0.1.0
		 * 
		 * @param string $uri	The image to use
		 * @param string $base	(optional) The base path to append the image to. Default '' (will use stylesheet directory)
		 * 
		 * @return string
		 */
		function ucdf_get_favicon( string $uri, string $base = '' ) : string {
			if( empty( $base ) ){
				$base = get_stylesheet_directory_uri();
			}
			return apply_filters( 'ucdf_get_favicon', sprintf( '<link rel="icon" type="image/png" href="%s">', $uri ) );
		}


	/**
	 * Outputs the site footer
	 * 
	 * @since 0.1.0
	 * 
	 * @return void
	 */
	function ucdf_footer() : void {
		wp_footer(); ?>
		</body>
		</html><?php
	}

	/**
	 * Outputs the site header
	 * 
	 * @since 0.1.0
	 * @since 0.3.0 Added support for Google Tag Manager, text directionality, preventing title/description being set
	 * 
	 * @param array $args (optional) Arguments (see wp_parse_args call for options)
	 * 		@type string	$title				The title to use. Default ucdf_get_title()
	 * 		@type string	$description		The description/tagline to use. Default get_bloginfo( 'description' )
	 * 		@type string	$favicon			The URI of the favicon to use. Default get_theme_file_uri( 'favicon.png' )
	 *		@type string	$gtag_id			ID for GTAG (implements Google Tag Manager if set)
	 * 		@type string	$og_image			The URI of the image to use for OpenGraph sharing. Default get_theme_file_uri( 'img/og-image.png' )
	 * 		@type string	$body_class			Extra class(es) to add to the opening body tag. Default ''
	 * 		@type string	$dir				Text directionality of the document body. Default '' (don't explicitly set one)
	 * 		@type bool		$set_title			Whether to set the document title in this function
	 * 		@type bool		$set_description	Whether to set the document description in this function
	 * 
	 * @return void
	 */
	function ucdf_header( array $args = [] ) : void {

		$r = wp_parse_args( $args, array(
			'title'				=> ucdf_get_title(),					// Title of the page
			'description'		=> get_bloginfo( 'description' ),		// Description/tagline of the site
			'favicon'			=> get_theme_file_uri( 'favicon.png' ),	// Favicon path to use (relative to theme directory)
			'og_image'			=> 'img/og-image.png',					// OpenGraph image to use (relative to theme directory)
			'body_class'		=> '',									// Additional body classes to add
			'gtag_id'			=> '',									// ID for GTAG (implements Google Tag Manager if set
			'set_title'			=> true,								// Whether to set the document title in this function
			'set_description'	=> true,								// Whether to set the document description in this function
			'dir'				=> ''									// Text directionality (only used if set)
		));
		$dir = $r['dir'] ? sprintf( ' dir="%s"', esc_html( $r['dir'] ) ) : ''

		?><!DOCTYPE html>
		<html <?php language_attributes(); ?>>
		<head>
			<?php if( !empty( $r['gtag_id'] ) ): ?>
				<script async src="https://www.googletagmanager.com/gtag/js?id=<?= esc_attr( $r['gtag_id']) ?>"></script>
				<script>
					window.dataLayer = window.dataLayer || [];
					function gtag(){
						dataLayer.push( arguments );
					}
					gtag( 'js', new Date() );
					gtag( 'config', '<?= esc_js( $r['gtag_id'] ) ?>' );
				</script>
			<?php endif;?>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<meta name="referrer" content="origin-when-cross-origin">
			<?php if( $r['set_description'] ): ?>
				<meta name="description" content="<?php echo $r['description'] ?>">
			<?php endif; ?>

			<meta property="og:title" content="<?php $r['title'] ?>">
			<meta property="og:url" content="<?php trailingslashit( $_SERVER['REQUEST_URI'] ) ?>">
			<meta property="og:type" content="website">
			<meta property="og:description" content="<?php echo $r['description'] ?>">
			<meta property="og:image" content="<?php get_theme_file_uri( $r['og_image'] ); ?>">
			<meta property="og:locale" content="en_GB">

			<?php ucdf_favicon( $r['favicon'] ); ?>
			<?php wp_head() ?>

			<?php if( $r['set_title'] ): ?>
				<title><?php echo $r['title'] ?></title>
			<?php endif; ?>
		</head>
		<body <?php body_class( $r['body_class'] ) ?><?= $dir ?>><?php
			wp_body_open();
	}

	/**
	 * Display a Font Awesome icon (requires FA to be enqueued)
	 * 
	 * @since 0.1.0
	 * 
	 * @see ucdf_get_icon
	 * 
	 * @param string $icon @see ucdf_get_icon
	 * @param string $class @see ucdf_get_icon
	 * @param string $type @see ucdf_get_icon
	 * 
	 * @return void
	 */
	function ucdf_icon( string $icon, string $class = '', string $type = ''  ) : void {
		echo ucdf_get_icon( $icon, $class, $type );
	}

		/**
		 * Return markup for a Font Awesome icon (requires FA to be enqueued)
		 * 
		 * @since 0.1.0
		 * 
		 * @param string $icon	The name of the icon to show
		 * @param string $class (optional) Any additional classes to add to the icon. Default empty string
		 * @param string $type	(optional) The icon type to use. Default empty string (use UCDF default)
		 * 
		 * @return string The icon markup
		 */
		function ucdf_get_icon( string $icon, string $class = '', string $type = '' ) : string {

			global $ucdf;

			if( !wp_script_is( $ucdf->fa_kit_handle, 'done' ) ){
				trigger_error( 'UCDF: Icons cannot be used without a valid FontAwesome kit. Use the ucdf_config filter to set fa_kit_id to enable icons', E_USER_NOTICE );
				return '';
			}

			// Work out default type
			if( $type == '' ){
				$type = ucdf_get_config( 'icon_type' );
			}

			return apply_filters( 'ucdf_get_icon',
				sprintf( '<i class="fa%s fa-%s%s"></i>',
					$type,
					$icon,
					empty( $class ) ? '' : sprintf( ' %s', $class )
				)
			);
		}

	/**
	 * Output the theme's logo, or empty string if none exists
	 * 
	 * @since 0.1.0
	 * 
	 * @see ucdf_get_theme_logo
	 * 
	 * @return void
	 */
	function ucdf_theme_logo() : void {
		echo ucdf_get_theme_logo();
	}

	/**
	 * Get markup for the theme's logo
	 * 
	 * First checks if there's a custom logo implemented.
	 * Second checks if the standard file location is usable
	 * 
	 * @since 0.1.0
	 * @since 0.1.1 Fixed theme file logos & dropped dot in ucdf_logo_file_extensions values
	 * @since 0.2.0 Includes SVGs in logo search
	 * 
	 * @return string
	 */
	function ucdf_get_theme_logo() : string {

		$logo = '';

		// Get custom logo if there is one
		$logo_id = get_theme_mod( 'custom_logo' );
		if( $logo_id ){
			$logo = wp_get_attachment_image_url( $logo_id, 'full', false );
		}

		// Use file in theme if no custom logo
		if( !$logo ){

			// File types we look for by default
			$options = apply_filters( 'ucdf_logo_file_extensions', [
				'svg',
				'png',
				'jpg',
			]);

			// Look for logo, breaking at the first one found
			foreach( $options as $ext ){

				$ext_uri = sprintf( 'img/brand/logo.%s', $ext );

				$possible_logo = get_theme_file_path( $ext_uri );
				if( file_exists( $possible_logo ) ){
					$logo = get_theme_file_uri( $ext_uri );
					break; 
				}

			}
		}

		return apply_filters( 'ucdf_get_theme_logo', $logo );

	}

	/**
	 * Shortcut to generate standard header for page templates
	 * 
	 * @since 0.1.0
	 * @since 0.1.1 Only calls get_template_part if the desired php file exists
	 * 
	 * @return void;
	 */
	function ucdf_page_header() : void {
		get_header();
	
		do_action( 'ucdf_page_before' );

		if( locate_template( 'inc/content-page-before.php' ) ){
			get_template_part( 'inc/content', 'page-before' );
		}
	}

	/**
	 * Shortcut to generate standard footer for page templates
	 * 
	 * @since 0.1.0
	 * @since 0.1.1 Only calls get_template_part if the desired php file exists
	 * @since 0.3.0 Fixed locate_template call to look for correct "after" template
	 * 
	 * @return void
	 */
	function ucdf_page_footer() : void {
		if( locate_template( 'inc/content-page-after.php' ) ){
			get_template_part( 'inc/content', 'page-after' );
		}

		do_action( 'ucdf_page_after' );

		get_footer();
	}

	/**
	 * Get parts of an HTML tag using regex
	 * 
	 * @since 0.2.0
	 * 
	 * @param string $tag The HTML tag to break apart
	 * 
	 * @return stdClass The parts of the tag string
	 */
	function ucdf_regex_html_tag( string $tag ) : stdClass{

		// Divide tag into opening, content and closing
		// e.g. <span>something</span>
		// returns:
		//  0. <span>something</span>	(complete)
		//	1. <span>					(opening)
		//	2. something				(content)
		//	3. </span>					(closing)
		preg_match( '/(<[^>]+>)(.*)(<\/[^>]+>)/', $tag, $tag_parts );

		$tag = new stdClass();
		$tag->complete	= $tag_parts[0] ?? ''; 
		$tag->open		= $tag_parts[1] ?? '';
		$tag->content	= $tag_parts[2] ?? '';
		$tag->close		= $tag_parts[3] ?? '';

		return $tag;

	}

	/**
	 * Outputs a search form
	 * 
	 * @since 0.1.0
	 * 
	 * @see ucdf_get_search_form
	 * 
	 * @param array $args @see ucdf_get_search_form
	 * 
	 * @return void
	 */
	function ucdf_search_form( array $args = array() ) : void{
		echo ucdf_get_search_form( $args );
	}
		/**
		 * Return markup for the search form
		 * 
		 * @since 0.1.0
		 * 
		 * @param array $args (otional) Arguments for the form. Default empty array
		 * 
		 * @return string
		 */
		function ucdf_get_search_form( array $args = array() ) : string {
		
			// Parse arguments
			$r = wp_parse_args( $args, array(
				'class'			=> 'container-xl',
				'input_name'	=> 'search',
				'btn_type'		=> 'primary',
				'btn_text'		=> 'Search'
			));

			// Generate additional markup for the form
			$class = $r['class'] ? sprintf( ' class="%s"', $r['class'] ) : '';
			$query = get_search_query();
			$value = empty( $query ) ? '' : sprintf( ' value="%s"', $query );

			return sprintf(
				'<form role="search" method="get"%s action="%s">
					<div class="input-group">
						<label class="sr-only" for="%s">Search for:</label>
						<input type="search" id="%3$s" name="s" class="form-control" placeholder="Search"%s>
						<div class="input-group-append">
							<button class="btn btn-%s">%s</button>
						</div>
					</div>
				</form>',
				$class,
				home_url(),
				$r['input_name'],
				$value,
				$r['btn_type'],
				$r['btn_text']
			);
		}

	/**
	 * Display an SVG
	 * 
	 * @since 0.3.1
	 * 
	 * @param string $uri The URI of the SVG to include, with or without the .svg extension
	 * 
	 * @return void
	 */
	function ucdf_svg( string $uri ) : void {
		
		// Allow calling of this function without the extension
		if( '.svg' != substr( $uri, -4 ) ){
			$uri .= '.svg';
		}

		echo file_get_contents( $uri );
	}

	/**
	 * Display an SVG from within the theme
	 * 
	 * @since 0.3.1
	 * 
	 * @see ucdf_svg
	 */
	function ucdf_theme_svg( string $uri ) : void {
		ucdf_svg( sprintf( '%s/%s', get_stylesheet_directory(), $uri ) );
	}

	/**
	 * Outputs a formatted string as the document title
	 * 
	 * @since 0.1.0
	 * 
	 * @see ucdf_get_title
	 */
	function ucdf_title() : void {
		echo ucdf_get_title();
	}

		/**
		 * Retrieves a formatted string as the document title
		 * 
		 * @since 0.1.0
		 * 
		 * @return string The formatted title
		 */
		function ucdf_get_title() : string {
			return apply_filters( 'ucdf_get_title', sprintf( '%s | %s', get_bloginfo( 'name' ), ( is_front_page() ? get_bloginfo( 'description' ) : wp_title( '', false ) ) ) );
		}
