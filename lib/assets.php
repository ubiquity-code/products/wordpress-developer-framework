<?php
	/**
	 * Functions relating to enqueueing assets, their attributes etc.
	 * 
	 * @since 0.1.0
	 */

	/**
	 * Get a cache-busting version for the file. Last modified time is the best bet
	 * 
	 * @since 0.1.0
	 * 
	 * @param string $url The URL to get the version for
	 * 
	 * @return string The version to use (empty string on failure)
	 */
	function ucdf_asset_version( string $url ) : string {

		$uri = parse_url( $url );
		return apply_filters( 'ucdf_asset_version', $uri ? filemtime( $_SERVER['DOCUMENT_ROOT'] . $uri['path'] ) : '' );

	}

	/**
	 * Enqueue a versioned script (automatically uses asset version if no version provided)
	 * 
	 * @since 0.1.0
	 * @since 0.3.0 Added $var_key and $vars params to pass variables to localised JS script
	 * 
	 * @see wp_enqueue_script
	 * @see ucdf_asset_version
	 * 
	 * @param string	$handle		@see wp_enqueue_script
	 * @param string	$src		@see wp_enqueue_script
	 * @param array		$deps		@see wp_enqueue_script
	 * @param mixed		$ver		@see wp_enqueue_script
	 * @param bool		$in_footer	@see wp_enqueue_script
	 * @param string	$var_key	(optional) The key to use for the JS variables object
	 * @param array		$vars		(optional) Variables to pass to the script (@see https://blog.wplauncher.com/pass-php-values-to-a-javascript-file-in-wordpress/)
	 * 
	 * @return void
	 */
	function ucdf_enqueue_script( string $handle, string $src = '', array $deps = array(), $ver = false, bool $in_footer = false, string $var_key = '', array $vars = array() ){
		$version = $ver ?: ucdf_asset_version( $src );

		if( empty( $var_key ) || empty( $vars ) ){
			wp_enqueue_script( $handle, $src, $deps, $version, $in_footer );
		}else{
			wp_register_script( $handle, $src, $deps, $version, $in_footer );
			wp_localize_script( $handle, $var_key, $vars );
			wp_enqueue_script( $handle );
		}
	}


	/**
	 * Enqueue a versioned style (automatically uses asset version if no version provided)
	 * 
	 * @since 0.1.0
	 * 
	 * @see wp_enqueue_style
	 * @see ucdf_asset_version
	 * 
	 * @param string	$handle 	@see wp_enqueue_style
	 * @param string	$src		@see wp_enqueue_style
	 * @param array		$deps		@see wp_enqueue_style
	 * @param mixed		$ver		@see wp_enqueue_style
	 * @param string	$media		@see wp_enqueue_style
	 * 
	 * @return void
	 */
	function ucdf_enqueue_style( string $handle, string $src, array $deps = array(), $ver = false, string $media = 'all' ) : void {
		wp_enqueue_style( $handle, $src, $deps, $ver ?: ucdf_asset_version( $src ), $media );		
	}

	/**
	 * Add extra attributes to enqueued scripts
	 * 
	 * @since 0.1.0
	 * @since 0.3.0 Added support for boolean attributes
	 * 
	 * @see script_loader_tag
	 * 
	 * @param string $tag		@see script_loader_tag
	 * @param string $handle	@see script_loader_tag
	 * @param string $src		@see script_loader_tag
	 * 
	 * @return string The script tag, after any necessary processing
	 */
	function ucdf_script_extras( string $tag, string $handle, string $src ) : string {

		// Return tag as-is if we have no config to use
		$config = ucdf_get_config( 'script_extras' );

		if( !is_array( $config ) || !array_key_exists( $handle, $config ) ){
			return $tag;
		}

		// Extra markup
		$extra	= '';

		foreach( $config[$handle] as $attr => $val ){

			// Attribute key
			$extra .= sprintf( ' %s', $attr );

			// Only add the '="value"' part if it's not a bool with value 'true' (just the key otherwise)
			if( !is_bool( $val ) || !$val ){
				$extra .= sprintf( '="%s"', $val );
			}
			
		}

		return apply_filters( 'ucdf_script_extras', sprintf( '<script src="%s"%s></script>', $src, $extra ) );

	}
	add_filter( 'script_loader_tag', 'ucdf_script_extras', 10, 3 );

