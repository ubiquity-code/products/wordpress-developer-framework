<?php

	/**
	 * 'Safely' start a session (aka. only if one isn't already active)
	 * 
	 * @since 0.2.0
	 * 
	 * @return void
	 */
	function ucdf_session_start_safe() : void {
		
		if( !session_id() && !headers_sent() ){
			session_start();
		}

	}