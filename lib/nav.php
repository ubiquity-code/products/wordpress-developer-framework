<?php
	/**
	 * Funcations relating to WP nav menus, walkers, etc.
	 * 
	 * @since 0.1.0
	 */

	/**
	 * Construct a nav menu using Walker_Nav_Menu and UCDF filters
	 * 
	 * @since 0.1.0
	 * 
	 * @see Walker_Nav_Menu
	 * @see wp_nav_menu
	 * 
	 * @param array $args @see wp_nav_menu
	 * 
	 * @return void
	 */
	function ucdf_nav_menu( array $args = array() ) : void {

		$args['walker'] = new Walker_Nav_Menu;

		// Style-specific args
		switch( ucdf_get_config( 'menu_style' ) ){
			case 'bootstrap':

				// Which class(es) we need for this style
				$needed = 'nav';
				
				// Convert the passed-in set to an array
				$classes = array_filter( explode( ' ', $args['menu_class'] ?? '' ) );

				// Add the needed class if not already there
				if( !in_array( $needed, $classes ) ){
					$classes[] = $needed;
				}

				// Convert back to a string
				$args['menu_class'] = implode( ' ', $classes );
		}
		
		if( !isset( $args['fallback_cb'] ) ){
			$args['fallback_cb'] = function(){ echo '';  };
		}

		wp_nav_menu( $args );
	}

	/**
	 * Filters the CSS classes applied to a menu item's list item element.
	 *
	 * @since 0.1.0
	 * 
	 * @see nav_menu_css_class
	 *
	 * @param array				$classes Array of the CSS classes that are applied to the menu item's `<li>` element. @see nav_menu_css_class
	 * @param WP_Post			$item    The current menu item.
	 * @param array|stdClass	$args    An array of wp_nav_menu() arguments.
	 * @param int				$depth   Depth of menu item. Used for padding.
	 * 
	 * @return array The filtered array of classes
	 */
	function ucdf_nav_menu_css_class( array $classes, WP_Post $item, $args, int $depth ) : array {
		
		$custom = [];

		// Classes are based on chosen menu style
		switch( ucdf_get_config( 'menu_style' ) ){
			case 'bootstrap':
				$custom[] = 'nav-item'; 
		}

		return array_merge( $classes, $custom );
	}
	add_filter( 'nav_menu_css_class', 'ucdf_nav_menu_css_class', 10, 4 );

	/**
	 * Filters the HTML attributes applied to a menu item's anchor element.
	 *
	 * @since 0.1.0
	 * @since 0.3.0 Ensures class key existence + class is appended rather than set, to allow theme filters to add classes too.
	 * 
	 * @see nav_menu_link_attributes
	 *
	 * @param array $atts The HTML attributes applied to the menu item's `<a>` element, empty strings are ignored.
	 *
	 *     @type string $title        Title attribute.
	 *     @type string $target       Target attribute.
	 *     @type string $rel          The rel attribute.
	 *     @type string $href         The href attribute.
	 *     @type string $aria_current The aria-current attribute.
	 *
	 * @param WP_Post 			$item  The current menu item.
	 * @param array|stdClass	$args  An object of wp_nav_menu() arguments.
	 * @param int     			$depth Depth of menu item. Used for padding.
	 * 
	 * @return array The final attributes
	 */
	function ucdf_nav_menu_link_attributes( array $atts, WP_Post $item, $args, int $depth ) : array {

		// Attributes are based on chosen menu style
		switch( UCDF_CONFIG_MENU_STYLE ){
			case 'bootstrap':
				$custom = 'nav-link';

				if( isset( $atts['class'] ) ){
					$atts['class'] .= " $custom";
				}else{
					$atts['class'] = $custom;
				}
		}

		return $atts;

	}
	add_filter( 'nav_menu_link_attributes', 'ucdf_nav_menu_link_attributes', 10, 4 );