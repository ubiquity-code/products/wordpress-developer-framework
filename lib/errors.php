<?php
	/**
	 * Functions relating to WP_Error instances
	 * 
	 * @since 0.2.0
	 */

	/**
	 * Implode a WP_Error into a string of error codes
	 * 
	 * @since 0.2.0
	 * 
	 * @param WP_Error	$error	The error object
	 * @param string	$glue	(optional) Glue for the implosion. Default '|'
	 * 
	 * @return string The imploded string
	 */
	function ucdf_implode_error_codes( WP_Error $error, string $glue = '|' ){
		return implode( $glue, $error->get_error_codes() );
	}

	/**
	 * Implode a WP_Error into a string of error messages
	 * 
	 * @since 0.2.0
	 * 
	 * @param WP_Error	$error	The error object
	 * @param string	$glue	(optional) Glue for the implosion. Default '<br>'
	 * 
	 * @return string The imploded string
	 */
	function ucdf_implode_error_messages( WP_Error $error, string $glue = '<br>' ){
		return implode( $glue, $error->get_error_messages() );
	}
