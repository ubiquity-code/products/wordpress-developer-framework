<?php
	/**
	 * Functions relating to UI alerts
	 */

	/**
	 * Check whether there are any alerts queued (optionally in a given category)
	 * 
	 * @since 0.1.0
	 * 
	 * @param string $category (optional) The category to assign the alert to. Default ''
	 * 
	 * @return bool Whether any alerts exist
	 */
	function ucdf_alerts_exist( string $category = '' ) : bool {

		ucdf_session_start_safe();

		if( !isset( $_SESSION['alerts'] ) || empty( $_SESSION['alerts'] ) ){
			return false;
		}

		$alerts = $_SESSION['alerts'];

		// Count the alerts, limited to a category if necessary
		return boolval( empty( $category ) ? count( $alerts ) : array_reduce( $alerts, function( $total, $alert ) use ( $category ){
			if( isset( $alert->category ) && ( $alert->category == $category ) ){
				$total++;
			}
			return $total;
		}, 0 ));

	}

	/**
	 * Display all queued alerts (optionally in a given category)
	 * 
	 * @since 0.1.0	
	 * 
	 * @param string $category (optional) The category to assign the alert to. Default ''
	 * 
	 * @return void
	 */
	function ucdf_alerts( string $category = '' ) : void {

		ucdf_session_start_safe();

		if( ucdf_alerts_exist() ){

			// Loop over the alerts
			foreach( $_SESSION['alerts'] as $i => &$alert ){

				// If needed, validate category
				$category_valid = empty( $category ) || ( isset( $alert->category ) && ( $alert->category == $category ) );

				if( !isset( $alert->type ) || !isset( $alert->message ) ){

					// Remove the alert if it doesn't have a message or type
					unset( $alert );

				}elseif( $category_valid ){

					// Output the alert then remove it
					ucdf_alert( $alert );
					unset( $_SESSION['alerts'][$i] );

				}

			}

			$_SESSION['alerts'] = array_values( $_SESSION['alerts'] );

		}

	}
	

	/**
	 * Show a given alert
	 * 
	 * @since 0.1.0
	 * 
	 * @see ucdf_get_alert
	 * 
	 * @param object $alert The alert to show 
	 * 
	 * @return void
	 */
	function ucdf_alert( object $alert ) : void{
		echo ucdf_get_alert( $alert );
	}

	/**
	 * Return the markup for a given alert
	 *
	 * @since 0.1.0
	 * 
	 * @param object $alert The alert to return markup for
	 * 
	 * @return string The markup
	 */
	function ucdf_get_alert( object $alert ) : string{

		// Default to info type
		if( !isset( $alert->type ) ){
			$alert->type = 'danger';
		} 

		return sprintf(
			'<div class="alert alert-%1$s d-flex align-items-center">
				<p class="mb-0">%3$s</p>
			</div>',
			strtolower( $alert->type ),
			get_template_directory_uri(),
			$alert->message
		);
	}

	/**
	 * Get the last alert in the queue. Returns null on failure
	 *
	 * @since 0.1.0
	 * 
	 * @param bool $markup (optional) Whether to return markup (true) or the alert object (false). Default true
	 * 
	 * @return object|null The last alert, null if none exist
	 */
	function ucdf_get_last_alert( bool $markup = true ){

		ucdf_session_start_safe();

		if( ucdf_alerts_exist() ){
			$alert = end( $_SESSION['alerts'] );
			return $markup ? ucdf_get_alert( $alert ) : $alert;
		}else{
			return null;
		}
	}

	/**
	 * Clear out any existing alerts
	 * 
	 * @since 0.1.0
	 *
	 * @return void
	 */
	function ucdf_clear_alerts() : void {

		ucdf_session_start_safe();

		if( isset( $_SESSION['alerts'] ) ){
			unset( $_SESSION['alerts'] );
		}
	}
	
	/**
	 * Enqueue an alert
	 *
	 * @param object $alert
	 * 
	 * @return void
	 */
	function ucdf_add_alert( object $alert ) : void {

		ucdf_session_start_safe();

		if( isset( $alert->message ) ){

			if( !isset( $alert->type ) ){
				$alert->type = 'danger';
			}
            
			// Add the alert to the session array
			$_SESSION['alerts'][] = $alert;

			// Fix array keys
			$_SESSION['alerts'] = array_values( $_SESSION['alerts'] );

		}

	}
	
	/**
	 * Get an error message, displaying error codes to admin users
	 * 
	 * @since 0.2.0
	 * 
	 * @param string $message	The message to show
	 * @param string $code		The code to show to authorised users
	 * 
	 * @return string
	 */
	function ucdf_coded_message( string $message, string $code ) : string {

		// Default authorisation criteria
		$authorised = ucdf_is_admin_user();

		/**
		 * Allow developers to filter how a specific code's authorisation is granted
		 * 
		 * @since 0.2.0
		 * 
		 * @param bool $authorised Whether the user is authorised by default @see ucdf_is_admin_user
		 * 
		 * @return bool Authorised?
		 */
		$authorised = apply_filters( "ucdf_message_authorised_{$code}", $authorised );

		/**
		 * Allow developers to filter how any message authorisation is granted
		 * 
		 * @since 0.2.0
		 * 
		 * @param bool 		$authorised Whether the user is authorised by default
		 * @param string	$message	The message
		 * @param string	$code		The code for the message
		 * 
		 * @return bool Authorised?
		 */
		$authorised = apply_filters( 'ucdf_message_authorised', $authorised, $message, $code );

		return $authorised ? sprintf( '%s (%s)', $message, $code ) : $message;

	}