<?php
	/**
	 * Functions relating to UCDF config
	 * 
	 * @since 0.1.0
	 */

	/**
	 * Output a UCDF config value
	 * 
	 * @since 0.1.0

	 * @see ucdf_get_config
	 * 
	 * @param string $config_key @see ucdf_get_config
	 * 
	 * @return void
	 */
	function ucdf_config( string $config_key ) : void {
		echo ucdf_get_config( $config_key );
	}

		/**
		 * Get a UCDF config value
		 * 
		 * @since 0.1.0
		 * 
		 * @param string $config_key The key to look for (appended to the prefix)
		 * 
		 * @return mixed The value of the config, or null on failure
		 */
		function ucdf_get_config( string $config_key ) {

			global $ucdf;

			$key = strtoupper( $ucdf->config_prefix . $config_key );
			return defined( $key ) ? constant( $key ) : null;

		}