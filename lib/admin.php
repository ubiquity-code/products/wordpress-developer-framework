<?php
	/**
	 * Functions relating to the WP admin area
	 * 
	 * @since 0.1.0
	 */

	/**
	 * Check if a user is an admin user
	 * 
	 * The deafult "has administrator role or is super admin" logic can be filtered
	 * 
	 * @since 0.2.0
	 * 
	 * @return bool Whether the user is an admin user
	 */
	function ucdf_is_admin_user() : bool{
		/**
		 * Filter to set custom "is admin user" logic
		 * 
		 * @since 0.2.0
		 * 
		 * @param bool $is_admin Whether the user is considered an admin user
		 */
		return apply_filters( 'ucdf_is_admin_user', current_user_can( 'administrator' ) || is_super_admin() );
	}

	/**
	 * Remove 'Posts' from the nav menu
	 * 
	 * @since 0.1.0
	 * 
	 * @return void
	 */
	function ucdf_remove_menu_item_posts() : void {
		remove_menu_page('edit.php');
	}

	/**
	 * Remove 'Comments' from the nav menu
	 * 
	 * @since 0.1.0
	 * 
	 * @return void
	 */
	function ucdf_remove_menu_item_comments() : void {
		remove_menu_page( 'edit-comments.php' );
	}

	/**
	 * Restrict access to wp-admin by a custom boolean 
	 * 
	 * @since 0.2.0
	 * 
	 * @return void
	 */
	function ucdf_restrict_admin_access() : void {

		if( is_admin() ){
			
			/**
			 * Fires beore the redirect check to set access logic
			 * 
			 * @since 0.2.0
			 * 
			 * @param bool $granted Whether admin access is granted. Default true (all users can access)
			 */
			$access_granted = apply_filters( 'ucdf_admin_access', true );

			// Redirect if a non-admin user is trying to access an admin page. AJAX calls are made using an 'admin' script, so must account for that
			if( !$access_granted && ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ){
				
				if( is_user_logged_in() ){
					ucdf_logged_in_redirect();
				}else{
					wp_redirect( home_url( 'wp-login.php' ) );
					exit;
				}
			}

		}
	
	}
	add_action( 'init', 'ucdf_restrict_admin_access' );