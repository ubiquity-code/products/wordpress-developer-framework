<?php
	/**
	 * Perform a DB delta including foreign keys
	 * 
	 * @since 0.2
	 * 
	 * @param array $queries	The queries for dbDelta
	 * @param array $fk_config 	(optional) Foreign key config. Default empty (no FKs)
	 */
	function ucdf_db_delta( array $queries, array $fk_config = array() ){

		$db_delta = dbDelta( $queries );
		
		if( !empty( $fk_config ) ){
			ucdf_db_fk_delta( $fk_config );
		}

		return $db_delta;

	}

	/**
	 * DB delta of only foreign keys
	 * 
	 * @since 0.2
	 */
	function ucdf_db_fk_delta( array $config ){

		global $wpdb;

		// Loop tables
		foreach( $config as $table => $new_constraints ){

			// Get requested constraint names for this table
			$constraint_names = implode( "','", array_keys( $new_constraints ) );

			// Check which of these already exist
			$db_constraints = $wpdb->get_results(
				"SELECT CONSTRAINT_NAME, REFERENCED_TABLE_NAME
				FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS
				WHERE TABLE_NAME = '$table'
					AND CONSTRAINT_NAME IN ('$constraint_names')",
				OBJECT_K
			);

			// Build query for only non-existent constraints
			$qry_constraints = '';
			foreach( $new_constraints as $constraint_name => $constraint_config ){

				if( !array_key_exists( $constraint_name, $db_constraints ) ){
					
					if( !empty( $qry_constraints ) ){
						$qry_constraints .= ', ';
					}

					$qry_constraints .= 'ADD CONSTRAINT '.$constraint_name.' FOREIGN KEY ('.$constraint_config['field'].') REFERENCES '.$constraint_config['referenced_table'].'('.$constraint_config['referenced_field'].')';

				}

			}

			// Alter the table if there are constraints to add
			if( !empty( $qry_constraints ) ){
				$wpdb->query( "ALTER TABLE $table $qry_constraints" );
			}

		}

	}