<?php

//if( false ){
	
	/**
	 * Emulates the normal wp-login flow and jump in late enough to run logic just
	 * before the custom login page's template is called
	 * 
	 * @since 0.2.0
	 * 
	 * @see pre_get_posts
	 */
	function ucdf_login_form_action_hook(){

		if( ucdf_is_custom_login_page() ){
			/**
			 * On our custom page fire the same hook as wp-login does by
			 * default, before the custom tempalte's inclusion
			 *
			 * @since 0.2.0
			 * 
			 * @see login_form_{$action}
			 */
			do_action( sprintf( 'login_form_%s', ucdf_get_page_action( 'login' ) ) );

		}
	}
	add_action( 'template_redirect', 'ucdf_login_form_action_hook' );

	/**
	 * Whether the current page is the custom login page.
	 * 
	 * Note this will not work prior to the 'pre_get_posts' hook
	 * as it relies on knowing what the main queried object
	 * is/will be
	 * 
	 * @since 0.2.0
	 * 
	 * @return bool Whether the current page is the custom login page
	 */
	function ucdf_is_custom_login_page() : bool {
		return ucdf_using_custom_login_page() && is_page( ltrim( ucdf_get_login_page(), '/' ) );
	}

	/**
	 * Send error codes to a custom login page if in use
	 * 
	 * @since 0.2.0
	 * 
	 * @see authenticate
	 */
	function ucdf_after_login_validation( $user, string $username, string $password ){

		if( ucdf_using_custom_login_page() && ( 'POST' == $_SERVER['REQUEST_METHOD'] ) && is_wp_error( $user ) ){
			wp_redirect( ucdf_get_login_error_url( $user ) );
			exit;
		}
		
		return $user;

	}
	add_filter( 'authenticate', 'ucdf_after_login_validation', 100, 3 ); // Standard authentication is 99 and we want to run after that

	/**
	 * Create a URL including login errors
	 * 
	 * @since 0.2.0
	 * 
	 * @see ucdf_implode_error_codes
	 * 
	 * @param WP_Error	$error	The error object to get codes from
 	 * @param string	$action	The action to add to the URL
	 * 
	 * @return string The URL, including error query string
	 */
	function ucdf_get_login_error_url( WP_Error $error, string $action = '' ){
		return add_query_arg( 'error', ucdf_implode_error_codes( $error ), ucdf_get_login_url( $action ) );
	}

	/**
	 * Handle password resets
	 * 
	 * @since 0.2.0
	 */
	function ucdf_request_password_reset(){

		if( 'POST' == $_SERVER['REQUEST_METHOD'] ){

			$reset = retrieve_password();
		
			if( is_wp_error( $reset ) ){
				$destination = ucdf_get_login_error_url( $reset, 'lostpassword' );
			}else{
				$destination = add_query_arg( 'checkemail', 'confirm', ucdf_get_login_url() );
			}

			wp_redirect( $destination );
			exit;
		}

	}
	add_action( 'login_form_lostpassword', 'ucdf_request_password_reset' );
	
	/**
	 * Create an alert from the login errors present
	 * 
	 * @since 0.2.0
	 * 
	 * @see ucdf_get_login_error_message
	 * 
	 * @return void
	 */
	function ucdf_login_errors() : void {
		if( isset( $_REQUEST['error'] ) ){
			$error_messages = [];

			foreach( explode( '|', $_REQUEST['error'] ) as $code ){
				$error_messages[] = ucdf_get_login_error_message( sanitize_text_field( $code ) );
			}

			ucdf_alert((object)[
				'message' => implode( '<br>', $error_messages )
			]);
		}
	}

	/**
	 * Output a login error message from its code
	 * 
	 * @since 0.2.0
	 * 
	 * @see ucdf_get_login_error_message
	 * 
	 * @return void
	 */
	function ucdf_login_error_message( string $code ) : void {
		echo ucdf_get_login_error_message( $code );
	}

		/**
		 * Get a login error message from its code
		 * 
		 * @since 0.2.0
		 * 
		 * @param string $code The error code
		 * 
		 * @return string
		 */
		function ucdf_get_login_error_message( string $code ) : string {
			switch( $code ){
				case 'empty_username':
				case 'empty_password':
					$message = 'Both fields are required';
					break;

				case 'invalid_username':
				case 'incorrect_password':
				case 'invalidcombo':
					$message = 'Invalid username/password combination. Do you have caps lock on?';
					break;

				case 'invalid_email':
					$message = 'The e-mail address you entered is invalid';
					break;

				case 'retrieve_password_email_failure':
					$message = 'We were unable to send out your password reset email. Please contact us for assistance';
					break;

				case 'invalidkey':
					$message = 'You have used an invalid link. Please request a new one';
					break;

				case 'expiredkey':
					$message = 'The link you\'re using has expired. Please request a new one';
					break;

				case 'password_reset_mismatch':
					$message = 'The passwords you have entered do not match';
					break;

				case 'expired':
					$message = 'Your session has expired. Please log in again';
					break;

				case 'registerdisabled':
					$message = 'Account registration is currently disabled';
					break;


				default:
					$message = 'Unknown error';
			}

			/**
			 * Filter for code-specific theme customisations of the messages
			 * 
			 * @since 0.2.0
			 * 
			 * @param string $message The error message
			 */
			$message = apply_filters( "ucdf_get_login_error_{$code}", $message );

			/**
			 * Filter for a more all-encompassing theme customisation of the messages
			 * 
			 * @since 0.2.0
			 * 
			 * @param string $message	The latest error message
			 * @param string $code		The code we're checking
			 */
			return apply_filters( 'ucdf_get_login_error', $message, $code );
		}

	/**
	 * Output the login page url
	 * 
	 * @since 0.2.0
	 * 
	 * @see ucdf_get_login_url
	 * 
	 * @param string $action (optional) The action to include. Default ''
	 * 
	 * @return void
	 */
	function ucdf_login_url( string $action = '' ) : void {
		echo ucdf_get_login_url( $action );
	}

		/**
		 * Get the login page url
		 * 
		 * @since 0.2.0
		 * 
		 * @see ucdf_get_login_page
		 * 
		 * @return string
		 */
		function ucdf_get_login_url( string $action = '' ) : string {
			
			$url = home_url( ucdf_get_login_page() );

			if( $action ){
				return add_query_arg( 'action', $action, $url );
			}

			return $url;
		}

	/**
	 * Get the login page name
	 * 
	 * @since 0.2.0
	 * 
	 * @return string
	 */
	function ucdf_get_login_page(){
		/**
		 * Fires before the login redirect.
		 * 
		 * Sends no page by default (which will leave WP's standard login page
		 * intact)
		 * 
		 * @since 0.2.0
		 * 
		 * @param string $page The page URI to redirect to using @see home_url
		 */
		$login_page = apply_filters( 'ucdf_login_page', '' );

		return $login_page ?: 'wp-login.php';
	}

	/**
	 * Allow login page requests to be redirected to a custom page
	 * 
	 * @since 0.2.0
	 * 
	 * @return void
	 */
	function ucdf_login_page_redirect(){
		$login_page = ucdf_get_login_page();
		$action		= ucdf_get_page_action( 'login' );

		if( ucdf_using_custom_login_page() ){

			$page = basename($_SERVER['REQUEST_URI']);
			$default_page = "wp-login.php";

			$trimmed_login_page = ltrim( $login_page, '/' ); 

			/*
				Handle the request if we're on custom login page or the standard default wp-login (since we know
				already that we're using a custom login page, so wp-login actions we've hooked should definitely
				be processed)
			*/
			if( ucdf_is_custom_login_page() || ( substr( $page, 0, strlen( $default_page ) ) == $default_page ) && ( 'GET' ==  $_SERVER['REQUEST_METHOD'] ) ){

				/*
					If we're already on the login page then don't start an infinite loop. Don't start one
					anywhere, but for now specifically don't start one here
				*/
				if( ucdf_is_custom_login_page() && ( 'login' == $action ) && ( 'GET' == $_SERVER['REQUEST_METHOD'] ) ){
					return;
				}

				switch( $action ){
					case 'login':
					case 'bpnoaccess':
					case 'lostpassword':
					case 'rp': // Password reset
					case 'resetpass':
						if( is_user_logged_in() ){
							ucdf_logged_in_redirect();
						}else{
							$destination = ucdf_get_login_url();
						}
						break;

					case 'register':
					case 'confirm_admin_email':
					default:
						// Default/unhandled actions can be passed through to wp-login
						$destination = '';
				}

				// We we have a pwr to validate (or a lostpassword submission for which to set the cookies)
				if( in_array( $action, ['rp', 'resetpass'] ) ){
					$validation = ucdf_password_reset_cookies();

					if( is_wp_error( $validation ) ){

						$destination = add_query_arg(array(
							'login'	=> isset( $_REQUEST['rp_login' ] ) ? sanitize_text_field( $_REQUEST['rp_login'] ) : '',
							'key'	=> isset( $_REQUEST['rp_key'] ) ? sanitize_text_field( $_REQUEST['rp_key'] ) : ''
						), ucdf_get_login_error_url( $validation ) );

					}else{

						$destination = ( 'POST' == $_SERVER['REQUEST_METHOD'] ) ? ucdf_get_login_url() : '';
						$add_success = true;

					}
				}

				// Redirect if we've got a destination
				if( !empty( $destination ) ){
					if( !empty( $_SERVER['QUERY_STRING'] ) ){
						$destination .= "?{$_SERVER['QUERY_STRING']}";
					}
					
					if( isset( $add_success ) && $add_success ){
						$destination = add_query_arg( array( 'success' => true ), $destination );
					}

					wp_redirect( $destination );
					exit;
				}
			}

		}
	}
	add_action( 'login_form_login', 'ucdf_login_page_redirect' );
	add_action( 'login_form_rp', 'ucdf_login_page_redirect' );
	add_action( 'login_form_resetpass', 'ucdf_login_page_redirect', );

	/**
	 * Validate a password reset submission
	 * 
	 * @since 0.2.0
	 * 
	 * @return WP_Error|bool Returns true on success, WP_Error on failure
	 */
	function ucdf_password_reset_cookies(){

		$rp_cookie	= 'wp-resetpass-' . COOKIEHASH;
//		$wp_cookie	= 'wp-resetpass-' . COOKIEHASH; // The WP equivalent, so we can clear it later
		$key_login	= 'login';
		$key_key	= 'key';

		if( 'POST' == $_SERVER['REQUEST_METHOD'] ){

			if( 'POST' == $_SERVER['REQUEST_METHOD'] ){
				$key_login	= 'rp_' . $key_login;
				$key_key	= 'rp_' . $key_key;
			}

			$rp_login = isset( $_REQUEST[$key_login] ) ? sanitize_text_field( $_REQUEST[$key_login] ) : '';
			$rp_key	= isset( $_REQUEST[$key_key] ) ? sanitize_text_field( $_REQUEST[$key_key] ) : '';
			$cookie_value	= sprintf( '%s:%s', wp_unslash( $rp_login ), wp_unslash( $rp_key ) );
			$url			= parse_url( $_SERVER['HTTP_HOST'] );
			$errors 		= new WP_Error;

			if( empty( $rp_login ) || empty( $rp_key ) ){
				$errors->add( 'invalidkey', ucdf_get_login_error_message( 'invalidkey' ) );
				return $errors;
			}

			switch( $_SERVER['REQUEST_METHOD'] ){
				case 'GET':
					wp_mail(
						'james@ubiquitycode.co.uk',
						'Cookie:' . $cookie_value,
						'Cookie key:' . $rp_cookie . '\n' .
							'We\'re looking for: ' . $cookie_value
					);

					$done = setcookie( $rp_cookie, $cookie_value, time()+60*60*2, '/', home_url(), is_ssl(), true );

					return true;
					
				case 'POST':
					$password 			= isset( $_POST['pass1'] ) ? sanitize_text_field( $_POST['pass1'] ) : '';
					$password_confirm	= isset( $_POST['pass2'] ) ? sanitize_text_field( $_POST['pass2'] ) : '';
					$user				= get_user_by( 'login', $rp_login );

					global $wp_hasher;
					if ( empty( $wp_hasher ) ) {
						require_once ABSPATH . WPINC . '/class-phpass.php';
						$wp_hasher = new PasswordHash( 8, true );
					}
					$parts = explode( ':', $user->user_activation_key, 2 );

					$result = $wp_hasher->CheckPassword( $rp_key, end( $parts ) );

					if( !$result ){	
						$errors->add( 'invalidkey', ucdf_get_login_error_message( 'invalidkey' ) );
						return $errors;
					}
						
					if( empty( $password ) || empty( $password_confirm ) ){
						$errors->add( 'empty_password', ucdf_get_login_error_message( 'empty_password' ) );
						return $errors;
					}

					if( $password != $password_confirm ){
						$errors->add( 'password_reset_mismatch', ucdf_get_login_error_message( 'password_reset_mismatch' ) );
						return $errors;
					}

					if( $errors->has_errors() ){
						return $errors;
					}else{

						// Made it, phew! Reset the password and delete the damn cookie
						reset_password( $user, $password );
						setcookie( $rp_cookie, ' ', time() - YEAR_IN_SECONDS, '/', $url['hostname'], is_ssl(), true );
						//setcookie( $wp_cookie, ' ', time() - YEAR_IN_SECONDS, '/', $url['hostname'], is_ssl(), true );
						return true;

					}
					break;
			}

		}else{
			return true;
		}

		// Shouldn't be here
		return false;
	}

	/**
	 * Customise password reset email message (to circumvent the purged cookie issue)
	 * 
	 * @since 0.2.0
	 * 
	 * @see retrieve_password_message
	 */
	function ucdf_password_reset_message( string $message, string $key, string $user_login, WP_User $user_data ){

		// Nothing to do if not using custom login page	
		if( !ucdf_using_custom_login_page() ){
			return $message;
		}
		
		// Replace wp-login.php reference with the custom page
		$new_message = str_ireplace( 'wp-login.php', ltrim( ucdf_get_login_page(), '/' ), $message );

		return $new_message;

	}
	add_filter( 'retrieve_password_message', 'ucdf_password_reset_message', 10, 4 );


	/**
	 * Check if we're using a custom login page
	 * 
	 * @since 0.2.0
	 * 
	 * @return bool Whether a custom page is being used
	 */
	function ucdf_using_custom_login_page(){
		return '' != apply_filters( 'ucdf_login_page', '' );
	}

//}