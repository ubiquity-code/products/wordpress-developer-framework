<?php
	/**
	 * Functions relating to debugging
	 * 
	 * @since 0.1.0
	 */

	/**
	 * Dump and die, pretty-printing a variable
	 * 
	 * @since 0.1.0
	 * 
	 * @see ucdf_pre
	 * 
	 * @param mixed $var	@see ucdf_pre
	 * @param bool $types	@see ucdf_pre
	 * 
	 * @return void
	 */
	function ucdf_die_pre( $var, bool $types = false ) : void {
		ucdf_pre( $var, $types );
		die();
	}

	/**
	 * Pretty-print a variable for WP admin
	 * 
	 * @since 0.1.0
	 * 
	 * @see ucdf_pre
	 * 
	 * @param mixed $var	@see ucdf_pre
	 * @param bool $types	@see ucdf_pre
	 * 
	 * @return void
	 */
	function ucdf_admin_pre( $var, bool $types = false ) : void{
		echo '<div style="margin-left: 12rem">';
		ucdf_pre( $var, $types );
		echo '</div>';
	}

		/**
		 * Pretty-print a variable
		 * 
		 * @since 0.1.0
		 * 
		 * @param mixed $var The variable to print
		 * @param bool $types (optional) Whether to show types. Default false
		 */
		function ucdf_pre( $var, bool $types = false ) : void {
			echo '<pre>';
			if( $types ){
				var_dump( $var );
			}else{
				print_r( $var );
			}
			echo '</pre>';
			echo PHP_EOL;
		}

		/**
		 * Return a pretty-printed variable
		 * 
		 * @since 0.1.0
		 * 
		 * @see ucdf_pre
		 * 
		 * @param mixed $var	@see ucdf_pre
		 * @param bool	$types	@see ucdf_pre
		 * 
		 * @return string The pretty-print markup
		 */
		function ucdf_get_pre( $var, bool $types = false ) : string {
			ob_start();
				ucdf_pre( $var, $types );
			return ob_get_clean();
		}

	/**
	 * Display PHP errors (for certain conditions)
	 * 
	 * @since 0.3.0 
	 * 
	 * @param bool $condition (optional) Condition to check for whether to display errors. Defualt null (check @see is_super_admin)
	 * 
	 * @return void
	 */
	function ucdf_display_errors( bool $condition = null ) : void {
		$condition = $condition ?? is_super_admin(); // Default to super admins only

		if( $condition ){
			@ini_set( 'display_errors', 1 );
		}
	}

	/**
	 * Handle an error
	 * 
	 * @since 0.1.0
	 * 
	 * @param WP_Error $error The error to handle
	 * 
	 * @return void
	 */
	function ucdf_error( WP_Error $error ){
		if( WP_DEBUG ){
			ucdf_alert((object)[
				'message' => ucdf_get_pre( sprintf( 'UCDF Error: %s %s', $error->get_error_message(), ucdf_get_trace( $error->get_error_data()->exception ) ) )
			]);
		}else{
			ucdf_alert((object)[
				'message' => sprintf( 'An error occurred [Code %s]', $error->get_error_code() )
			]);
		}
	}

	/**
	 * Output a backtrace
	 * 
	 * @since 0.1.0
	 * 
	 * @see ucdf_get_trace
	 * 
	 * @param Exception $e @see ucdf_get_trace
	 * 
	 * @return void
	 */
	function ucdf_trace( Exception $e ) : void {
		echo ucdf_get_trace( $e );
	}

		/**
		 * Format an exception into a backtrace
		 * 
		 * @since 0.1.0
		 * 
		 * @param Exception $e The exception to format
		 *  
		 * @return string The formatted backtrace
		 */
		function ucdf_get_trace( Exception $e ) : string {

			$formatted = sprintf(
				'(%s:%d)
				<br>%s',
				$e->getFile(),
				$e->getLine(),
				$e->getTraceAsString()
			);

			return $formatted;
		}