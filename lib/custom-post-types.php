<?php
	/**
	 * Functions relating to custom post types
	 * 
	 * @since 0.1.0
	 */

	/**
	 * Get various forms of a string
	 * 
	 * @since 0.1.0
	 * @since 0.2.0 Lowercase name forms now use sanitize_title to handle spaces etc.
	 * 
	 * @param string $singular 	The singular string to get forms for
	 * @param array $args		(optional) Arguments to use. Default empty array
	 * 		@type string $plural Manually set the plural form to use
	 * 
	 * @return object Different forms of the original, singular name
	 */
	function ucdf_get_name_forms( string $singular, array $args = array() ) : object {

		// Get the plural form (append an 's' if nothing specified)
		$plural	= $args['plural'] ?? sprintf( '%ss', $singular );
		unset( $args['plural'] );

		// Lowercase forms
		$singular_lower = sanitize_title( strtolower( $singular ) );
		$plural_lower = sanitize_title( strtolower( $plural ) );

		$result = (object)compact( 'singular', 'plural', 'singular_lower', 'plural_lower' );

		return apply_filters( 'ucdf_get_name_forms', $result );

	}

	/**
	 * Helper function for registering custom things (+ their labels)
	 * 
	 * @since 0.1.0
	 * 
	 * @param string $type		The type of custom thing we're registering
	 * @param string $singular	The singular name of the thing being registered
	 * @param array $args		(optional) The arguments to use. Default empty array @see register_post_type @see register_taxonomy
	 * @param string $domain	(optional) Text domain to use. Default 'ucdf'
	 * 
	 * @return array The args to pass to the final registration
	 */
	function ucdf_help_register( string $type, string $singular, array $args = array(), string $domain = 'ucdf' ) : array {

		$names = ucdf_get_name_forms( $singular, $args );

		$default_labels = array(
			'add_new'						=> __( 'Add New', $domain ),
			'add_new_item'					=> __( "Add New $names->singular", $domain ),
			'add_or_remove_items'			=> __( "Add or remove $names->plural_lower", $domain ),
			'all_items'						=> __( "All $names->plural", $domain ),
			'archives'						=> __( "$names->singular archives", $domain ),
			'attributes'					=> __( "$names->singular attributes", $domain ),
			'back_to_items'					=> __( "Back to $names->plural"),
			'choose_from_most_used'			=> __( "Choose from most used $names->plural_lower", $domain ),
			'edit_item'						=> __( "Edit $names->singular", $domain ),
			'featured_image'				=> __( "$names->singular Featured Image", $domain ),
			'filter_items_list'				=> __( "Filter $names->singular_lower list", $domain ),
			'insert_into_item'				=> __( "Insert into $names->singular_lower", $domain ),
			'item_published'				=> __( "$names->singular published", $domain ),
			'item_published_privately'		=> __( "$names->singular published privately", $domain ),
			'item_reverted_to_draft'		=> __( "$names->singular reverted to draft", $domain ),
			'item_scheduled'				=> __( "$names->singular scheduled", $domain ),
			'item_updated'					=> __( "$names->singular updated", $domain ),
			'items_list_navigation'			=> __( "$names->singular list navigation", $domain ),
			'items_list'					=> __( "$names->singular list", $domain ),
			'menu_name'						=> __( $names->plural, $domain ),
			'most_used'						=> __( 'Most Used', $domain ),
			'name'							=> __( $names->plural, $domain ),
			'new_item'						=> __( "New $names->singular", $domain ),
			'new_item_name'					=> __( "New $names->singular name", $domain ),
			'no_terms'						=> __( "No $names->plural_lower", $domain ),
			'not_found'						=> __( "No $names->plural_lower found", $domain ),
			'not_found_in_trash'			=> __( "No $names->plural_lower found in Trash", $domain ),
			'parent_item'					=> __( "Parent $names->singular", $domain ),
			'parent_item_colon'				=> __( "Parent $names->plural:", $domain ),
			'popular_items'					=> __( "Popular $names->plural", $domain ),
			'remove_featured_image'			=> __( 'Remove featured image', $domain ),
			'search_items'					=> __( "Search $names->plural", $domain ),
			'separate_items_with_commas'	=> __( "Separate $names->plural_lower with commas", $domain ),
			'set_featured_image'			=> __( 'Set featured image', $domain ),
			'singular_name'					=> __( $singular, $domain ),
			'update_item'					=> __( "Update $names->singular", $domain ),
			'uploaded_to_this_item'			=> __( "Uploaded to this $names->singular_lower", $domain ),
			'use_featured_image'			=> __( 'Use as featured image', $domain ),
			'view_item'						=> __( "View $names->singular", $domain ),
			'view_items'					=> __( "View $names->plural", $domain ),
		);

		$labels = wp_parse_args( $args['labels'] ?? [], $default_labels );

		$r = array_merge( $args, compact( 'labels' ) );

		return $r;

	}


	/**
	 * Register a custom post type
	 * 
	 * @since 0.1.0
	 * 
	 * @param string $singular	The title case, singular form of the type's name
	 * @param arrray $args		(optional) Array of args. Default empty array @see register_post_type
	 * @param string $domain 	(optional) Text domain to use. Default 'ucdf'
	 * 
	 * @return WP_Post_Type|WP_Error The registered post type, error on failure
	 */
	function ucdf_register_post_type( string $singular, array $args = array(), string $domain = 'ucdf' ){
		$names = ucdf_get_name_forms( $singular, $args );
		return register_post_type( $names->plural_lower, ucdf_help_register( 'post_type', $singular, $args, $domain ) );
	}

	/**
	 * Register a custom taxonomy
	 * 
	 * @since 0.1.0
	 * 
	 * @param string		$singular		The title case, singular form of the taxonomy's name
	 * @param string		$taxonomy		The taxonomy's key
	 * @param string|array	$object_type	The ojbect(s) to associate the taxonomy with
	 * @param array			$args			(optional) Arguments for regigstering the taxonomy. Default empty array @see register_taxonomy
	 * @param string		$domain			(optional) Text domain to use. Default 'ucdf'
	 * 
	 * @return WP_Taxonomy|WP_Error The registered taxonomy, error on failure
	 */
	function ucdf_register_taxonomy( string $singular, string $taxonomy, $object_type, array $args = array(), string $domain = 'ucdf' ){
		return register_taxonomy( $taxonomy, $object_type, ucdf_help_register( 'taxonomy', $singular, $args, $domain ) );
	}
	/**
	 * Display a CPT archive based on the current page title
	 *
	 * @since 0.1.1 
	 * @since 0.3.0 Added $args param to pass args to WP_Query
	 *
	 * @param string	$cpt	(optional). The post_type of the CPT to use. Default is current page slug
	 * @param array		$args	(optional). Arguments to pass to WP_Query. post_type is ignored (uses the CPT)
	 * 
	 * @return void
	 */
	function ucdf_cpt_archive( string $cpt = '', array $args = array() ){

		if( !$cpt ){
			global $post;
			$cpt = $post->post_name;
		}

		$r = array_merge( $args, array(
			'post_type' => $cpt
		));

		$cpt_query = new WP_Query( $r );

		if( $cpt_query->have_posts() ):
			while( $cpt_query->have_posts() ): $cpt_query->the_post();
				get_template_part( 'inc/content', "single-$cpt-archive" );
			endwhile;
		endif;

		wp_reset_postdata();
	}