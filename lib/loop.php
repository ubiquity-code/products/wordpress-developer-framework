<?php
	/**
	 * Functions relating to The Loop
	 * 
	 * @since 0.1.0
	 */

	/**
	 * Set "the post" for the loop context
	 * 
	 * @since 0.1.0
	 * 
	 * @see setup_postdata
	 * 
	 * @param mixed $new_post @see setup_postdata
	 * 
	 * @return bool|WP_Error The success bool/error for the execution
	 */
	function ucdf_set_the_post( $new_post ) {

		if( !is_int( $new_post ) && !( $new_post instanceof WP_Post ) ){
			return new WP_Error( 'UCDF-101', sprintf( 'The value passed to %s must be a WP_Post or the ID of one', __FUNCTION__ ), (object)array(
				'exception' => new \Exception
			));
		}

		global $post;
		$post = $new_post;

		return setup_postdata( $post );
	}