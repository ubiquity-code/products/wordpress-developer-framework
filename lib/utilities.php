<?php

/**
 * Returns boolean value in Yes/No format
 * 
 * @since 0.3.0
 * 
 * @param bool $var				The variable
 * @param bool $single			(optional) Whether to only return a single letter. Default false
 * @param bool $strict_false	(optional) Whether to use strict (===) comparison for 'false'. Default false (don't use it)
 * 
 * @return string The Yes/No format
 */
function ucdf_yes_no( bool $var, bool $single = false, bool $strict = false ) : string {
	if( $strict ){
		$output = ( $var === false ) ? 'No' : 'Yes';
	}else{
		$output = ( $var ? 'Yes' : 'No' );
	}

	if( $single ){
		$output = substr( $output, 0, 1 );
	}

	return $output;
}