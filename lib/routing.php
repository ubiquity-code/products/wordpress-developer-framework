<?php
	/**
	 * Force a 404 (for restricted areas etc.)
	 * 
	 * @since 0.1.0
	 */
	function ucdf_404(){
		global $wp_query;

		$wp_query->set_404();
		status_header( 404 );
		get_template_part( '404' );
		exit();
	}

	/**
	 * Get the 'action' request var for the page
	 * 
	 * Most commonly used for custom login pages.
	 * 
	 * @since 0.2.0
	 * 
	 * @param string $default (optional) The default action to use if none is found. Default ''
	 * 
	 * @return string The page action
	 */
	function ucdf_get_page_action( string $default = '' ) : string {
		return isset( $_REQUEST['action'] ) ? sanitize_text_field( $_REQUEST['action'] ) : $default;
	}

	/**
	 * Load libraries from inside the theme directory
	 * 
	 * @since 0.1.0
	 * @since 0.2.0 Param can now be a string for single-path use
	 * 
	 * @see ucdf_load_libraries
	 * 
	 * @param array|string $paths A directory path as a string, or an array of directory paths, to load in
	 * 
	 * @return void
	 */
	function ucdf_load_theme_libraries( $paths ) : void {
		if( !is_array( $paths ) ){
			$paths = [ $paths ];
		}
		foreach( $paths as $path ){
			ucdf_load_libraries( sprintf( '%s/%s', get_stylesheet_directory(), $path ) );
		}
	}

	/**
	 * Where to redirect a logged-in user accessing a custom login page
	 * 
	 * @since 0.2.0
	 * 
	 * @return void
	 */
	function ucdf_logged_in_redirect(){

		if( is_user_logged_in() ){
			/**
			 * Fires before the logged-in redirect to customise the destination
			 * 
			 * @since 0.2.0
			 * 
			 * @param $destination The destination for the redirect using @see home_url. Default '/' (homepage)
			 */
			$destination = apply_filters( 'ucdf_logged_in_redirect', '/' );

			wp_safe_redirect( home_url( $destination ) );
			exit;
		}
	}

	/**
	 * Display a theme-specific URL
	 * 
	 * @since 0.1.0
	 * 
	 * @see get_theme_file_uri
	 * 
	 * @param string $relative_url The relative URL
	 * 
	 * @return void
	 */
	function ucdf_theme_url( string $relative_url ) : void {
		echo get_theme_file_uri( $relative_url );
	}


	/**
	 * Display a theme-specific path
	 * 
	 * @since 0.1.0
	 * 
	 * @see get_theme_file_path
	 * 
	 * @param string $relative_path The relative path
	 * 
	 * @return void
	 */
	function ucdf_theme_path( string $relative_path ) : void {
		echo get_theme_file_path( $relative_path );
	}
