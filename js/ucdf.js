$ = $ || jQuery;

function ucdfCSSVar( key ){

	// Return null for a blank function call
	if( typeof( key ) === 'undefined' ){
		return null;
	}

	// Allow specifying keys with or without the leading dashes
	if( key.substring( 0, 2 ) != '--' ){
		key = `--${key}`;
	}

	return getComputedStyle( document.documentElement ).getPropertyValue( key );
}

function ucdfData( $el, key, defaultValue ){
	if( typeof( defaultValue ) === 'undefined' ){
		defaultValue = '';
	}

	return $el.data( `ucdf-${key}` ) ?? defaultValue;
}

function ucdfIsMobile(){
	return window.innerWidth < parseInt( ucdfCSSVar( 'breakpoint-md' ) );
}

$(function(){

	// Clickable elements
	$( '[data-ucdf-clickable]' ).each(function( idx, el ){
		$this = $( el );

		$this.on( 'click', function(){
			window.location.href = $( this ).data( 'ucdf-clickable' );
		});
	});

	// Controllers
	$('.controller, [data-ucdf-controls]').on('click', function(e){
		e.preventDefault();

		var controller = $( this );
		/* 
			Get all the properties of the control in place
			controlled:			Element being controlled by this
			controlMethod:		What type of thing do with the element
			controlGroup:		The group that the controlled element belongs to
			controllerGroup:	The group that this belongs to
		*/
		var controlled		= $( '#' + ucdfData( controller, 'controls' ) );
		var controlMethod	= ucdfData( controller, 'control-method', 'slide' );
		var controlClass	= ucdfData( controller, 'control-class' );
		var controlGroup	= ucdfData( controller, 'control-group' );
		var controllerGroup	= ucdfData( controller, 'controller-group' );

		switch(controlMethod){

			// Class toggling
			case 'class':
				controlled.toggleClass( controlClass );
				break;

			// Slide/default
			case 'slide':
			default:
				
				if( controlGroup === '' ){ 

					// Not part of a group - do one slide
					controlled.toggle();

				}else{
					
					// Part of a group, only act if the controlled element isn't the currently-shown element
					if( controlled.css( 'display' ) === 'none' ){					

						$( '[data-control-group="' + controlGroup + '"]' ).each(function(){

							if( $( this ).css( 'display' ) !== 'none' ){

								// Slide the currently-shown element in the group
								$( this ).slideToggle();
							
							}

						});

						// Slide the controlled element
						controlled.slideToggle();
					
						if( controllerGroup !== '' ){

							// If the controller is in a group, switch the 'active' classes
							$( '[data-controller-group="' + controllerGroup + '"].active' ).removeClass( 'active' );
							$( this ).addClass( 'active' );

						}
					}		
				
				}
				
				break;

		}

	});

});