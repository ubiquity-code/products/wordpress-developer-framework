<?php
/**
 * Ubiquity Code Developer Framework
 * 
 * @package UCDF
 * @author Ubiquity Code
 * @copyright 2020 Ubiquity Code
 * @license GPL-2.0-or-later
 * 
 * @wordpress-plugin
 * Plugin Name: Ubiquity Code Developer Framework
 * Plugin URI: https://www.ubiquitycode.co.uk
 * Description: A collection of helper functions/tools for WP theme developers
 * Version: 0.3.1
 * Requires at least 5.7.0
 * Requires PHP: 7.3
 * Author: Ubiquity Code
 * Author URI: https://www.ubiquitycode.co.uk
 * License: GPL v2 or later
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: uc-dev-framework
 */

if( !class_exists( 'UCDF' ) ){

	/**
	 * @since 0.1.0
	 * 
	 * @var string The current plugin version
	 */
	define( 'UCDF_VERSION', '0.3.1' );

	/**
	 * @since 0.1.0
	 * 
	 * @var string The slug of the plugin
	 */
	define( 'UCDF_SLUG', 'ucdf' );

	/**
	 * @since 0.1.0
	 * 
	 * @var string The slug of the plugin's update transient
	 */
	define( 'UCDF_PLUGIN_TRANSIENT_UPDATE_SLUG', 'uc_update_plugin_' . UCDF_SLUG  );

	/**
	 * @since 0.1.0
	 * 
	 * @var string Base URL for other URLs
	 */
	define( 'UCDF_HOST_URL', 'https://www.ubiquitycode.co.uk' );

	/**
	 * @since 0.1.0
	 * 
	 * @var string The host URL for the plugin
	 */
	define( 'UCDF_PLUGIN_URL', sprintf( '%s/wp/plugins/%s', UCDF_HOST_URL, UCDF_SLUG ) );

	/**
	 * @since 0.1.0
	 * 
	 * @var string The slug to use for the plugin in the filesystem
	 */
	define( 'UC_FS_SLUG', 'uc-dev-framework' );


	// Get debug functions so the class can use them
	include_once __DIR__ . '/lib/debug.php';

	/**
	 * Load all non-dot, non-index PHP files from a given directory (declared
	 * outside of the UCDF class definition so that themes and the UCDF class
	 * can share the function). Note that this function leverages PHP's
	 * DirectoryIterator and the order which files are included will match
	 * that of DirectoryIterator
	 * 
	 * @since 0.1.0
	 * 
	 * @param string $path Path to the folder to load libraries from
	 * 
	 * @return void
	 */
	function ucdf_load_libraries( string $path ) : void {

		$dir = new DirectoryIterator( $path );

		foreach( $dir as $file ){
			if( !$file->isDot() && ( $file->getExtension() == 'php' ) && !($file->getFilename() == 'index' ) ){
				include_once $file->getPathname();
			}
		}

	}

	/**
	 * Check if a given remote is valid
	 * 
	 * @since 0.1.0
	 * 
	 * @param mixed $remote The remote (or its error object)
	 * 
	 * @return bool Whether the remote is valid
	 */
	function ucdf_remote_is_valid( $remote ) : bool {
		return !is_wp_error( $remote ) && isset( $remote['response']['code'] ) && ( $remote['response']['code'] == 200 ) && !empty( $remote['body'] );
	}

	/**
	 * Set the plugin's update transient
	 * 
	 * @since 0.1.0
	 * 
	 * @param object|array $remote
	 * 
	 * @return void
	 */
	function ucdf_set_plugin_update_transient( $remote ) : void {
		ucdf_set_update_transient( UCDF_PLUGIN_TRANSIENT_UPDATE_SLUG, $remote );
	}

	/**
	 * Set an update transient
	 * 
	 * @since 0.1.0
	 * @since 0.3.1 Reduced caching time 12h -> 6h
	 * 
	 * @param string		$slug	The slug of the transient
	 * @param object|array	$remote The remote to use
	 */
	function ucdf_set_update_transient( string $slug, $remote ) : void {
		set_transient( $slug, $remote, 10800 );
	}

	/**
	 * Main UCDF class for the plugin
	 * 
	 * @since 0.1.0
	 */
	class UCDF{

		/**
		 * Config being used.
		 * 
		 * Defaults:
		 * 	@type string $icon_type		Type of FA icons to use. Default 'l' (light)
		 * 	@type string $menu_style	Style to use for menus. Default '' (WordPress)
		 * 	@type string $script_extras	Extra attributes for enqueued scripts. Default array() (none)
		 *  @type string $fa_kit_id		ID of the FontAwesome kit to use for icons
		 * 
		 * @since 0.1.0
		 * 
		 * @var array
		 */
		private $config = array(
			'icon_type'		=> 'l',
			'menu_style'	=> 'bootstrap',
			'script_extras'	=> array(
				'ucdf-font-awesome' => array(
					'crossorigin' => 'anonymous'
				)
			)
		);

		/**
		 * Prefix for config constant names
		 * 
		 * @since 0.1.0
		 * 
		 * @var string
		 */
		public $config_prefix = 'UCDF_CONFIG_';

		/**
		 * The handle to use for enqueueing a Font Awesome kit
		 * 
		 * @since 0.1.0
		 * 
		 * @var string
		 */
		public $fa_kit_handle = 'ucdf-font-awesome';

		/**
		 * Uninstall hook for the plugin
		 * 
		 * @since 0.1.0
		 * 
		 * @return void
		 */
		public static function uninstall() : void {
			// Nothing to do for now
		}

		/**
		 * Constructor for the class
		 * 
		 * @since 0.1.0
		 */
		public function __construct(){

			register_activation_hook( __FILE__, array( $this, 'activate' ) );
			register_deactivation_hook( __FILE__, array( $this, 'deactivate' ) );
			register_uninstall_hook( __FILE__, array( self::class, 'uninstall' ) );

			// Include libraries
			$this->loadLibraries();

			// Add WP actions
			$this->addActions();
		}

		/**
		 * Activation hook for the plugin
		 * 
		 * @since 0.1.0
		 * 
		 * @return void
		 */
		public function activate() : void {
			// Nothing to do for now
		}

		/**
		 * Deactivation hook for the plugin
		 * 
		 * @since 0.1.0
		 * 
		 * @return void
		 */
		public function deactivate() : void {
			// Nothing to do for now
		}

		/**
		 * Add the necessary WP actions
		 * 
		 * @since 0.1.0
		 * 
		 * @return void
		 */
		private function addActions() : void {

			/**
			 * Get config when ready
			 * 
			 * Allows theme developers to specify config
			 * 
			 * @since 0.1.0
			 * 
			 * @param array $defaults The default values
	 		 * 
			 * 		@type string $icon_type		Type of FA icons to use. Default 'l' (light)
			 * 		@type string $menu_style	Style to use for menus. Default '' (WordPress)
			 * 		@type string $script_extras	Extra attributes for enqueued scripts. Default array() (none)
			 *
			 *
			 */
			add_action( 'wp_loaded', function(){
				$config = array_merge( $this->config, apply_filters( 'ucdf_config', $this->config ) );

				// Generate the constants
				foreach( $config as $prop => $val ){
					$constant = sprintf( '%s%s', $this->config_prefix, strtoupper( $prop ) );
					
					if( !defined( $constant ) ){
						define( $constant, $val );
					}
				}
			});

			/**
			 * Enqueue the necessary plugin assets
			 * 
			 * @since 0.1.0
			 * 
			 * @see wp_enqueue_scripts
			 */
			add_action( 'wp_enqueue_scripts', function(){

				// Main JS script
				wp_enqueue_script( 'ucdf-script', sprintf( '%sjs/ucdf.js', plugin_dir_url( __FILE__ ) ), array( 'jquery' ), UCDF_VERSION, true );

				$fa_kit = ucdf_get_config( 'FA_KIT_ID' );
				if( $fa_kit ){
					// Font Awesome kit
					wp_enqueue_script( $this->fa_kit_handle, sprintf( 'https://kit.fontawesome.com/%s.js', $fa_kit ) );
				}

			});

			/**
			 * Enable custom updates for the plugin
			 * 
			 * @since 0.1.0
			 * @since 0.1.1 Returns banners and icons in result
			 * 
			 * @see plugins_api
			 * 
			 * @param false|object|array	$res	@see plugins_api
			 * @param string				$action	@see plugins_api
			 * @param object				$args	@see plugins_api
			 * 
			 * @return object|false Injected plugin info, where appropriate (false otherwise)
			 */
			add_filter( 'plugins_api', function( $res, string $action, object $args ){

				// Bail if not getting plugin info, or not working on this plugin
				if( ( 'plugin_information' !== $action) || ( UCDF_SLUG !== $args->slug ) ){
					return false;
				}

				// Check the cache (get from remote if nothing exists)
				if( false == $remote = get_transient( UCDF_PLUGIN_TRANSIENT_UPDATE_SLUG ) ){

					$remote = wp_remote_get( sprintf( '%s/info', UCDF_PLUGIN_URL ), array(
						'timeout' => 10,
						'headers' => array(
							'Accept' => 'application/json'
						)
					));

					// Use the 12-hour cache if it's valid
					if( ucdf_remote_is_valid( $remote ) ){
						ucdf_set_plugin_update_transient( $remote );
					}

				}

				// Inject the right info if we can
				if( ucdf_remote_is_valid( $remote ) ){

					$result = json_decode( $remote['body'] );
					$res	= (object)array(
						'name'			=> $result->name,
						'slug'			=> $result->slug,
						'version'		=> $result->version,
						'tested'		=> $result->tested,
						'requires'		=> $result->requires,
						'author'		=> sprintf( '<a href="%s" title="Author\'s website">Ubiquity Code</a>', UCDF_HOST_URL ),
						'download_link'	=> $result->download_url,
						'trunk'			=> $result->download_url,
						'requires_php'	=> $result->requires_php,
						'last_updated'	=> $result->last_updated,
						'banners'		=> $result->banners,
						'icons'			=> (array)$result->icons,
						'sections'		=> array(
							'description' 		=> $result->sections->description,
							'installation'		=> $result->sections->installation,
							'changelog'			=> $result->sections->changelog
						)
					);

					if( isset( $result->sections->screenshots ) ){
						$res->sections['screenshots'] = $result->sections->screenshots;
					}

					if( isset( $result->banners ) ){
						$res->banners = array(
							'low'	=> $result->banners->low,
							'high'	=> $result->banners->high
						);
					}

					if( isset( $result->versions ) ){
						$res->versions = $result->versions;
					}

					unset( $remote_is_valid );
					return $res;
				}

				// If we get here, we failed
				unset( $remote_is_valid );
				return false;

			}, 20, 3 );

			/**
			 * Push plugin update info to WP transients
			 * 
			 * @since 0.1.0
			 * @since 0.1.1 Returns banners and icons in result
			 * 
			 * @see site_transient_update_plugins
			 * 
			 * @param object $transient @see site_transient_update_plugins
			 * 
			 * @return object @see site_transient_update_plugins
			 */
			add_filter( 'site_transient_update_plugins', function( $transient ){
				$original_transient = $transient;

				if( empty( $transient->checked ) ){
					return $transient;
				}

				// Try to use cache first
				if( false == $remote = get_transient( UCDF_PLUGIN_TRANSIENT_UPDATE_SLUG ) ){
				
					$remote = wp_remote_get( sprintf( '%s/info', UCDF_PLUGIN_URL ), array(
						'timeout' => 10,
						'headers' => array(
							'Accept' => 'application/json'
						)
					));

					if( ucdf_remote_is_valid( $remote ) ){
						ucdf_set_plugin_update_transient( $remote );
					}
					
				}

				if( $remote ){

					if( is_wp_error( $remote ) ){
						return $original_transient;
					}

					$result = json_decode( $remote['body'] );

					if( $result && version_compare( UCDF_VERSION, $result->version, '<' ) && version_compare( $result->requires, get_bloginfo( 'version' ), '<=' ) ){
						$res = (object)array(
							'slug'			=> UCDF_SLUG,
							'plugin'		=> sprintf( '%1$s/%1$s.php', UC_FS_SLUG ),
							'new_version'	=> $result->version,
							'tested'		=> $result->tested,
							'requires'		=> $result->requires,
							'package'		=> $result->download_url,
							'banners'		=> $result->banners,
							'icons'			=> (array)$result->icons
						);
						$transient->response[$res->plugin] = $res;
					}

					return $transient;

				}

			});

			/**
			 * Clear cache after updating
			 * 
			 * @since 0.1.0
			 */
			add_action( 'upgrader_process_complete', function( $upgrader_object, $options ){
				if( ( 'update' == $options['action'] ) && ( 'plugin' == $options['type'] ) ){
					delete_transient( UCDF_PLUGIN_TRANSIENT_UPDATE_SLUG );
				}
			}, 10, 2 );


			add_filter( 'http_request_host_is_external', function( $external, $host, $url ){

				// Permit Ubiquity's dev URL
				if( 'ubiquity.local' == $host ){
					return true;
				}

				return $external;

			}, 10, 3 );

		}

		/**
		 * Load function libraries
		 * 
		 * @since 0.1.0
		 * 
		 * @return void
		 */
		private function loadLibraries() : void {
			ucdf_load_libraries( sprintf( '%slib', plugin_dir_path( __FILE__ ) ) );
		}

	}


	// Initialise
	$_GLOBALS['ucdf'] = $ucdf = new UCDF();

}